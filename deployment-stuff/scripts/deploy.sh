#!/bin/bash

systemctl status -n 0 instagram-clone.service
sudo mv /opt/instagram-clone/deployment-stuff/systemd/instagram-clone.service /etc/systemd/system/instagram-clone.service
sudo systemctl daemon-reload
sudo systemctl restart instagram-clone.service
systemctl status -n 0 instagram-clone.service

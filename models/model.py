from pymongo import MongoClient,UpdateMany
from flask import session
from bson.objectid import ObjectId

client=MongoClient()

db=client['instagram']

def user_exists(username):
	result = db.users.find_one({'username':username})
	return result

def add_user(user):
	db.users.insert(user)
	db.posts.insert({'user_id':user_info(user['username'])['_id'],'posts':[]})

def user_info(username):
	query={'username':username}
	result = db.users.find_one(query)
	return result

def post_info(username):
	res1 = db.users.find_one({'username':username})
	query={'user_id':res1['_id']}
	result = db.posts.find_one(query)
	return result

def a_post_info(username,pn):
	posts = db.posts.find_one({'user_id':user_info(username)['_id']})['posts']
	for post in posts:			
		if post['post-number']==int(pn):
			return post

def save_post(post_data):
	db.posts.update({'user_id':user_info(session['username'])['_id']},{'$addToSet':{'posts':post_data}})

def save_comment(username, post_number, comment_info):
	posts = db.posts.find_one({'user_id':user_info(username)['_id']})['posts']
	for post in posts:
		if post['post-number']==post_number:
			post['comments'].append(comment_info)
			break
	db.posts.update({'user_id':user_info(username)['_id']},{'$set':{'posts':posts}})

def change_dp(post_path):
	db.users.update({'username':session['username']},{'$set':{'dp':post_path}})

def user_list():
	result=db.users.find({'username':{'$ne':session['username']},'followers':{'$ne':session['username']}})
	return result

def follow_user(username):
	db.users.update({'username':session['username']},{'$addToSet':{'following':{'$each':[username]}}})
	db.users.update({'username':username},{'$addToSet':{'followers':{'$each':[session['username']]}}})
	
def update_profile(username='',bio=''):
	if username!='':
		previous=session['username']
		db.users.update({'username':session['username']},{'$set':{'username':username}})
		db.users.bulk_write([UpdateMany({'followers':previous},{'$set':{'followers.$':username}})])
		db.users.bulk_write([UpdateMany({'following':previous},{'$set':{'following.$':username}})])
	if bio!='':
		db.users.update({'username':session['username']},{'$set':{'bio':bio}})

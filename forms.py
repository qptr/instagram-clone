from wtforms import Form,StringField,PasswordField,SubmitField,BooleanField
from wtforms.validators import DataRequired,Length,Email,EqualTo

class SignupForm(Form):
	username = StringField('Username',validators=[DataRequired(),Length(min=2,max=20)])
	email = StringField('Email',validators=[DataRequired(),Email()])
	password=PasswordField('Password',validators=[DataRequired(),Length(min=3,max=20)])
	confirm_password=PasswordField('Confirm Password',validators=[DataRequired(),EqualTo('password')])
	submit=SubmitField('Sign Up')


class LoginForm(Form):
	username = StringField('Username',validators=[DataRequired(),Length(min=2,max=20)])
	password=PasswordField('Password',validators=[DataRequired(),Length(min=3,max=20)])
	submit=SubmitField('Login')

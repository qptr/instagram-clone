#!/usr/bin/python3

from flask import Flask,flash,render_template,redirect,url_for,request,session
from models.model import *
from forms import SignupForm,LoginForm
from werkzeug.utils import secure_filename
import os

app_path = '/opt/instagram-clone'
UPLOAD_FOLDER = os.path.join(app_path,'static')
ALLOWED_EXTENSIONS = set(['png', 'jpg', 'jpeg'])

app=Flask(__name__)
app.config['SECRET_KEY']=os.environ.get('SECRET_KEY')
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER


def allowed_file(filename):
    return '.' in filename and filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS

#home
@app.route('/')
def home():
	return render_template('home.html')

#login
@app.route('/login',methods=['GET','POST'])
def login():
	form=LoginForm(request.form)
	if request.method=='POST' and form.validate():
		username=form.username.data
		password=form.password.data
		result=user_exists(username)
		if result:
			if password!=result['password']:
				return "invalid password"
			session['username']=username
			return redirect(url_for('home'))
		return 'user does not exist'
	return render_template('login.html',form=form)

#signup
@app.route('/signup',methods=['GET','POST'])
def signup():
	form=SignupForm(request.form)
	userinfo={}
	#import pdb;pdb.set_trace()
	if request.method=='POST':
		username=form.username.data
		password=form.password.data
		if user_exists(username):
			return 'user already exists'
	if form.validate():
			userinfo['username']=username
			userinfo['password']=password
			userinfo['bio']='placeholder bio'
			userinfo['email']=form.email.data
			userinfo['dp']='/static/default_profile_picture.png'
			userinfo['followers']=[]
			userinfo['following']=[]
			add_user(userinfo)
			return redirect(url_for('login'))
	return render_template('signup.html',form=form)

#explore
@app.route('/explore',methods=['GET','POST'])
def explore():
	userlist=user_list()
	return render_template('explore.html',user_list=userlist)

#follow user
@app.route('/follow',methods=['GET','POST'])
def follow():
	if request.method=='POST':
		follow_user(request.form['name'])
	return redirect(url_for('explore'))

#profile
@app.route('/<user_name>',methods=['GET','POST'])
def profile(user_name):
	user = user_info(user_name)
	posts = post_info(user_name)
	return render_template('profile.html',user=user,posts=posts)

#post
@app.route('/<user_name>/post/<post_no>', methods=['GET','POST'])
def a_post(user_name, post_no):
	post = a_post_info(username=user_name,pn = post_no)
	return render_template('post.html', username=user_name, post=post)

#comment under post
@app.route('/<user_name>/post/<post_no>/comment', methods=['GET','POST'])
def comment(user_name, post_no):
	a_comment = {}
	if request.method == 'POST':
		a_comment['made_by'] = session['username']
		a_comment['text'] = request.form['comment']
		save_comment(user_name, int(post_no), a_comment)
	return redirect(url_for('a_post',user_name=user_name,post_no=post_no))

#upload post, renders hmtl
@app.route('/upload/sd23f&4', methods=['GET', 'POST'])
def upload_file():
	#import pdb;pdb.set_trace()
	if request.method=='POST':
		return render_template('upload.html')
	return redirect(url_for('profile'))

#upload post, actual work done
@app.route('/upload/ew1rty&td', methods=['GET', 'POST'])
def save_file():
	post_data = {}
	if request.method == 'POST':
		if 'file' in request.files:
			file = request.files['file']
			if file and allowed_file(file.filename):
				with open(os.path.join(app_path,'post-number.txt'),'r') as f:
					pn = int(f.read())+1
				with open(os.path.join(app_path,'post-number.txt'),'w') as f:
					f.write(str(pn))
				save_path = os.path.join(app.config['UPLOAD_FOLDER'],str(user_info(session['username'])['_id']))
				if not os.path.exists(save_path):
					os.mkdir(save_path)
				new_name='post-'+str(pn)+'.'+file.filename.rsplit('.', 1)[1].lower()
				file.save(os.path.join(save_path,new_name))
				post_data['path'] = os.path.join('/static',str(user_info(session['username'])['_id']),new_name)
				post_data['post-number'] = pn
				post_data['comments'] = []
				post_data['likes'] = []
				if request.form['caption']:
					post_data['caption'] = request.form['caption']
				else:
					post_data['caption'] = ''
				save_post(post_data)
		user=user_info(session['username'])
		posts=post_info(session['username'])
		return redirect(url_for('profile',user_name=session['username']))
	return redirect(url_for('home'))

#edit profile
@app.route('/<user_name>/edit',methods=['GET','POST'])
def editprofile(user_name):
	if request.method=='POST':
		return render_template('editprofile.html',user=user_info(session['username']))
	return redirect(url_for('home'))

#update profile after editing
@app.route('/<user_name>/update',methods=['GET','POST'])
def updateprofile(user_name):
	if request.method=='POST':
		if 'file' in request.files:
			file = request.files['file']
			if file and allowed_file(file.filename):
				save_path = os.path.join(app.config['UPLOAD_FOLDER'],str(user_info(session['username'])['_id']),'dp')
				if not os.path.exists(save_path):
					os.makedirs(save_path)
				new_name='profile_picture'+'.'+file.filename.rsplit('.', 1)[1].lower()
				file.save(os.path.join(save_path,new_name))
				change_dp(os.path.join('static',str(user_info(session['username'])['_id']),'dp',new_name))
		if request.form['username']:
			username = request.form['username']
			if user_exists(username):
				return 'username is already taken'
			update_profile(username=username)
			session['username']=username
		if request.form['bio']:
			update_profile(bio=request.form['bio']) 
	return redirect(url_for('profile',user_name=session['username']))

#logout
@app.route('/logout')
def logout():
	session.clear()
	return redirect(url_for('home'))


app.run(host='0.0.0.0')
